<?php

if (!defined('VARNISH_BAN_URLS_LOG_WATCHDOG_TYPE')) {
  define('VARNISH_BAN_URLS_LOG_WATCHDOG_TYPE', 'varnish_ban_urls');
}

// Import file that includes functions to operate with varnish administration console.
module_load_include('module', 'varnish', 'varnish');

/**
 * Implements hook_menu().
 */
function varnish_ban_urls_menu() {

  $menus['admin/content/cache/varnish-ban-urls'] = array (
    'title' => t('Limpiar URLs de cache'),
    'description' => t('Eliminar URLs de la cache de Varnish.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array (
      'varnish_ban_urls_form'
    ),
    'access arguments' => array (
      'ban urls from varnish'
    ),
    'access callback' => 'user_access',
    'type' => MENU_NORMAL_ITEM,
  ); 

  return $menus; 
}

/*
 * Handles the submit of the form where the user set the URLs he wants to ban from cache.
 */
function varnish_ban_urls_form_submit($form, &$form_state) {
  
  if($form['#id'] == 'varnish-ban-urls-form') {
  
    // One URL per line
    $urls = explode(PHP_EOL, $form_state['values']['ban-urls']);
    
    global $base_url;
    $host = parse_url($base_url)['host'];
    
    $commands = array();
    foreach ($urls as $url) {
      $commands[] = "ban req.http.host ~ $host && req.url ~ $url";
    }
    
    // Execute the list of commands in the varnish administration console
    $execution_results = _varnish_terminal_run($commands);
    
    // Parse the execution result of every command to detect if anyone failed.
    $execution_results = array_values($execution_results);
    $executed_commands = array_keys($execution_results[0]);
    foreach ($executed_commands as $command) {
	  try{
        $pos = strrpos($command, '~');
        if($pos !== False) {
          $banned_url = substr($command, $pos + 1);
          if($execution_results[0][$command]['code'] == 200){
            drupal_set_message(t("'$banned_url' eliminado correctamente de cach&eacute;"));		  
		    watchdog(VARNISH_BAN_URLS_LOG_WATCHDOG_TYPE, t('"%URL" eliminada correctamente de cache.'), array('%URL' => $banned_url), WATCHDOG_INFO);		  
          } else {
            $error_message = $execution_results[0][$command]['msg'];
            drupal_set_message(t("'$banned_url' no puedo ser limpiado de cach&eacute;. $error_message"), 'error');		  
  		    watchdog(VARNISH_BAN_URLS_LOG_WATCHDOG_TYPE, t('"%URL" no pudo ser eliminada de cache.'), array('%URL' => $banned_url), WATCHDOG_ERROR);		  
          }
        }
	  } catch (Exception $e) {
        watchdog_exception(VARNISH_BAN_URLS_LOG_WATCHDOG_TYPE, $e, '%mensaje', array('%mensaje' => t('Error eliminando URL de cache.')));
      }
    }
  }
}

/*
 * Builds the form to collect URLs from the user.
 */
function varnish_ban_urls_form($form, &$form_state) {

  $form['information'] = array(
    '#markup' =>  '<p>' . t('Introducir en la caja de texto las URLs que quiera eliminar de la cach&eacute, cada URL en una l&iacutenea distinta.') .
    '<br />' . t('Las URLs no deben incluir el nombre del dominio, por ejemplo, si desea eliminar "http://www.example.com/ruta/eliminar" deber&aacute introducir "/ruta/eliminar".') .
    '<br />' . t('Si por ejemplo desea eliminar de cache todos los ficheros con extensi&oacuten "pdf" puede hacerlo introduciendo ".pdf" como URL.') . '<p/>',
  );

  $form['ban-urls'] = array(
    '#type' => 'textarea',
    '#title' => t('URLs'),
    '#description' => t('Lista de urls a eliminar de la cache.'),
    '#rows' => 10, 
    '#default_value' => "",
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Eliminar de cache'),
  );

  return $form;
}


/**
 * Implements hook_permission().
 */
function varnish_ban_urls_permission() {
  return array (
    'ban urls from varnish' => array (
      'title' => t('Permiso para eliminar URLs de Varnish'),
      'description' => t('Permite a los usuarios limpiar URLs de la cach&eacute; de Varnish del sitio.')
    ),
  );
}
